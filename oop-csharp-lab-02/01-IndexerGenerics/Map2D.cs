﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace IndexerGenerics
{
    public class Map2D<TKey1, TKey2, TValue> : IMap2D<TKey1, TKey2, TValue>
    {
        /*
         * Suggerimento: come struttura dati interna per la memorizzazione dei valori della mappa
         * si suggerisce di utilizzare un Dictionary generico contenente come chiave una tupla di due
         * valori (le chiavi della mappa) e come valore il valore della mappa.
         * 
         * Esempio:
         * private IDictionary<Tuple<TKey1,TKey2>,TValue> values;
         */
        private IDictionary<Tuple<TKey1, TKey2>, TValue> values;

        public Map2D()
        {
            values = new Dictionary<Tuple<TKey1, TKey2>, TValue>();
        }

        public void Fill(IEnumerable<TKey1> keys1, IEnumerable<TKey2> keys2, Func<TKey1, TKey2, TValue> generator)
        {
            /*
             * Nota: si verifichi il funzionamento del metodo Invoke() su oggetti di classe Func
             */
             foreach(TKey1 key1 in keys1)
             {
                foreach(TKey2 key2 in keys2)
                {
                    values.Add(new Tuple<TKey1, TKey2>(key1, key2), generator.Invoke(key1,key2));
                }
            }
        }

        public bool Equals(IMap2D<TKey1, TKey2, TValue> other){

            /**foreach(var value in other.GetElements()){
                values.get
            }**/
            throw new NotImplementedException();

        }

        public TValue this[TKey1 key1, TKey2 key2]{
            /*
             * Suggerimento: si noti che sulla classe Dictionary è definito un indicizzatore per i valori di chiave
             */

            get => values[new Tuple<TKey1,TKey2>(key1,key2)];

            set => values[new Tuple<TKey1, TKey2>(key1, key2)] = value;
        }

        public IList<Tuple<TKey2, TValue>> GetRow(TKey1 key1)
        {
            throw new NotImplementedException();
        }

        public IList<Tuple<TKey1, TValue>> GetColumn(TKey2 key2)
        {
            throw new NotImplementedException();
        }

        public IList<Tuple<TKey1, TKey2, TValue>> GetElements()
        {
            IEnumerable<Tuple<TKey1, TKey2, TValue>> queryResult =
            from elem in values      
            select new Tuple< TKey1, TKey2, TValue >(elem.Key.Item1,elem.Key.Item2,elem.Value);

            IList<Tuple<TKey1, TKey2, TValue>> result = new List<Tuple<TKey1, TKey2, TValue>>(queryResult);
            return result;
        }

        public int NumberOfElements
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public override string ToString()
        {
            string stri = "";
            foreach(KeyValuePair<Tuple<TKey1, TKey2>, TValue> elem in values){
                stri += "" +elem.ToString() + "\n";
            }
            return stri;
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
